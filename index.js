const flaker = require('./flaker.js')();
const changesets = require('diff-json');
const EventEmitter = require('events');
const strsum = require('./strsum.js');

require('should');

const SMWorld = {};


/*
 * Polar coordinate system, used
 * in every in game region.
 */
SMWorld.AnyPolarPoint = class AnyPolarPoint {
    constructor(dist, angle) {
        if (dist < 0)
            return new SMWorld.AnyPolarPoint(-dist, angle + Math.PI);

        this.dist = dist;
        this.angle = angle - Math.floor(angle / Math.PI / 2) * Math.PI * 2;

        if (dist == 0) {
            this.x = 0;
            this.y = 0;
        }

        else {
            this.x = Math.cos(angle) * dist;
            this.y = Math.sin(angle) * dist;
        }
    }

    scale(s) {
        return SMWorld.AnyPolarPoint.fromCartesian(this.x * s, this.y * s);
    }

    toString() {
        return `(x=${this.x},y=${this.y})`;
    }

    toData() {
        return {
            dist: this.dist,
            angle: this.angle
        };
    }

    static fromData(d) {
        return new SMWorld.AnyPolarPoint(d.dist, d.angle);
    }

    distance(other) {
        return Math.sqrt(Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2));
    }

    static fromCartesian(x, y) {
        return new AnyPolarPoint(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)), Math.atan2(y, x));
    }

    neg() {
        return new AnyPolarPoint(-this.dist, this.angle);
    }

    dot(other) {
        return this.x * other.x + this.y * other.y;
    }

    angleDot(other) {
        return Math.acos(this.dot(other) / (this.dist * other.dist));
    }

    normalized() {
        return new SMWorld.AnyPolarPoint(1, this.angle);
    }

    static fromAngle(theta) {
        return new SMWorld.AnyPolarPoint(1, theta);
    }

    sub(coords) {
        return this.translate(coords.neg());
    }

    translate(coords) {
        return AnyPolarPoint.fromCartesian(this.x + coords.x, this.y + coords.y);
    }

    translateCartesian(x, y) {
        return this.translate({ x: x, y: y });
    }

    rotate(theta) {
        return new AnyPolarPoint(this.dist, this.angle + theta);
    }
};

// Polar coordinate tester.
new SMWorld.AnyPolarPoint(13, 0).x.should.be.exactly(13);
new SMWorld.AnyPolarPoint(13, 0).y.should.be.exactly(0);

let t = new SMWorld.AnyPolarPoint(14.1421, Math.PI / 4);
t = t.translate(SMWorld.AnyPolarPoint.fromCartesian(10, 15));

t.x.should.be.approximately(20, 0.0001);
t.y.should.be.approximately(25, 0.0001);

t.dist.should.be.approximately(Math.sqrt(Math.pow(20, 2) + Math.pow(25, 2)), 0.0001);
t.neg().x.should.be.approximately(-20, 0.0001);
t.neg().y.should.be.approximately(-25, 0.0001);

Math.sqrt(t.dot(t)).should.be.approximately(t.dist, 0.0001);

SMWorld.AnyPolarPoint.fromCartesian(2, 2).angle.should.be.approximately(Math.PI / 4, 0.0001);
SMWorld.AnyPolarPoint.fromCartesian(2, 2).dist.should.be.approximately(Math.sqrt(2) * 2, 0.0001);

function addType(cls, opts = {}) {
    cls.id = flaker();
    SMWorld.typeIndex.set(cls.id, cls);

    cls.sounds = opts.sounds || {};

    return cls;
}

SMWorld.typeIndex = new Map();

/*
 * Now we are at the Regions themselves!
 */
SMWorld.Entity = addType(class Entity extends EventEmitter {
    constructor(name, location, radius, solid, integrity, id = null) {
        super();
        
        this.destroyed = false;
        this.radius = radius;
        this.location = location;
        this.solid = solid;
        this.id = id || flaker();
        this.name = name;
        this.integrity = integrity;
        this.typeId = this.constructor.id;

        SMWorld.Entity.all.set(this.id, this);
    }

    tick() {

    }

    damage(points, source) {
        this.integrity -= points;

        this.emit('status event', `You get damaged by ${source != null && this.canSee(source) ? source.name : 'something'}!`);

        if (source != null)
            SMWorld.playSound(this, points, `${source.name} attacking ${this.name}...`);
            
        else
            SMWorld.playSound(this, points, `a ${['painful', 'agonizing', 'screeching', 'damaged'][Math.floor(5 * Math.random())]} sound from what seems to be ${this.name}...`);

        if (this.integrity <= 0)
            this.destroy(points);
    }

    destroy(volume = 63) {
        this.destroyed = true;
        SMWorld.Entity.all.delete(this.id);
        
        if (volume > 0 && this.constructor.sounds.destroyed != null)
            SMWorld.playSound(this, volume, this.constructor.sounds.destroyed[Math.floor(this.constructor.sounds.destroyed.length * Math.random())]);
    }
    
    undestroy() {
        this.destroyed = false;
        SMWorld.Entity.all.set(this.id, this);
    }
    
    static resetAll() {
        SMWorld.Entity.all = new Map();
    }

    postInit() {
        this.checkPos();
    }

    toData() {
        return {
            type: this.typeId,
            radius: this.radius,
            location: this.location.toData(),
            solid: this.solid,
            id: this.id,
            name: this.name,
            integrity: this.integrity,
            audition: this.audition || null,
            props: this.extraProps()
        };
    }

    extraProps() {
        return {};
    }

    static fromData(d) {
        return SMWorld.typeIndex.get(d.type)._fromData(d);
    }

    static _fromData(d) {
        let e = new Entity(d.name, SMWorld.AnyPolarPoint.fromData(d.location), d.radius, d.solid, d.integrity, d.id);
        if (d.audition != null) e.audition = d.audition;
        if (d.props.moveDest != null) e.moveDest = SMWorld.AnyPolarPoint.fromData(d.props.moveDest);
        if (d.props.angleDest != null) e.angleDest = d.props.angleDest;

        return e;
    }

    touches(other) {
        return this.location.distance(other.location) <= this.radius + other.radius;
    }

    setLocation(newLoc) {
        this.location = newLoc;
        
        SMWorld.Entity.all.forEach((entity, id) => {
            if (id === this.id) return;

            if (this.touches(entity)) {
                this.bump(entity);
                entity.bumped(this);
            }
        });

        this.checkPos();
    }

    checkPos() {
        this.traveling = false;
    }

    bump(other) {
        if (this.bumping) return;

        if (this.solid && other.solid) {
            let newLoc = this.location.translate(other.location.neg());
            newLoc = new SMWorld.AnyPolarPoint(this.radius + other.radius + 2, newLoc.angle);

            this.bumping = true;
            this.setLocation(other.location.translate(newLoc));
            this.bumping = false;

            if (this.angle != null)
                this.angle = newLoc.angle;
        }
    }

    bumped() {

    }
});

SMWorld.Entity.all = new Map();

SMWorld.LocalEntity = addType(class LocalEntity extends SMWorld.Entity {
    constructor(name, location, region, radius, solid, integrity, id = null) {
        super(name, location, radius, solid, integrity, id);
        this.region = region;
    }

    destroy(volume = 63) {
        super.destroy(volume);
        this.getRegion().content.delete(this.id);
    }

    undestroy() {
        super.undestroy();
        this.getRegion().content.set(this.id, this);
    }

    static resetAll() {
        
    }

    postInit() {
        super.postInit();
        this.getRegion().content.set(this.id, this);
    }

    getRegion() {
        return SMWorld.Region.all.get(this.region);
    }
    
    extraProps() {
        let dp = super.extraProps();
        dp.region = this.region;
        return dp;
    }

    static _fromData(d) {
        let e = new SMWorld.LocalEntity(d.name, SMWorld.AnyPolarPoint.fromData(d.location), d.props.region, d.radius, d.solid, d.integrity, d.id);
        if (d.audition != null) e.audition = d.audition;

        return e;
    }

    checkPos() {
        super.checkPos();

        if (!this.getRegion().inside(this))
            this.getRegion().fit(this);
    }

    localPos() {
        return this.getRegion().globalToLocal(this.location);
    }
});

SMWorld.AngledLocalEntity = addType(class AngledLocalEntity extends SMWorld.LocalEntity {
    constructor(name, location, region, radius, solid, integrity, angle, fov = Math.PI / 2, groundSpeed = 20, id = null) {
        super(name, location, region, radius, solid, integrity, id);

        this.groundSpeed = groundSpeed;
        this.angle = angle;
        this.angle -= Math.floor(this.angle / Math.PI / 2) * Math.PI * 2;
        this.fov = fov;
        this.moveDest = new SMWorld.AnyPolarPoint(0, 0);
        this.angleDest = 0;
    }

    tick(dt) {
        super.tick(dt);

        let mw = new SMWorld.AnyPolarPoint(Math.min(this.groundSpeed * dt, this.moveDest.dist), this.moveDest.angle);
        
        this.moveDest = this.moveDest.sub(mw);
        this.setLocation(this.location.translate(mw));

        if (isNaN(this.angle) || isNaN(this.angleDest)) {
            console.warn(`-------\nNaN found in ${this.name}'s angular parameters!\nType: ${this.constructor.name}\n-------`);
            this.angle = 0;
            this.angleDest = 0;
        }

        if (this.angleDest != 0) {
            if (Math.abs(this.angleDest) < (Math.PI / 3) * dt) {
                this._addAngle(this.angleDest);
                this.angleDest = 0;
            }
            
            else {
                let da = Math.sign(this.angleDest) * (Math.PI / 3) * dt;

                this._addAngle(da);
                this.angleDest -= da;
            }
        }
    }

    _addAngle(da) {
        this.setAngle(this.angle + da);
    }

    addAngle(da) {
        this.angleDest += da;
    }

    setAngle(a) {
        this.angle = a;
        this.angle -= Math.floor(this.angle / Math.PI / 2) * Math.PI * 2;
    }

    canSee(entity, fov=this.fov) {
        let a = this.relAngle(entity);

        let viewField = Math.tan(fov / 2);
        let dist = this.location.distance(entity.location);
        
        return Math.abs(a) <= Math.atan(viewField + entity.radius / dist);
    }

    relAngle(entity) {
        let a = this.angleTo(entity) - this.angle;
        a = (a * 180 / Math.PI + 180);
        a = (a - Math.floor(a / 360) * 360) - 180;
        a = a * Math.PI / 180;

        return a;
    }

    move(forward = 0, rightward = 0) {
        let fw = new SMWorld.AnyPolarPoint(forward, this.angle);
        let rw = new SMWorld.AnyPolarPoint(rightward, this.angle + Math.PI / 2);
        let mw = fw.translate(rw);
        
        this.moveDest = this.moveDest.translate(mw);

        return mw;
    }

    _move(forward = 0, rightward = 0) {
        let fw = new SMWorld.AnyPolarPoint(forward, this.angle);
        let rw = new SMWorld.AnyPolarPoint(rightward, this.angle + Math.PI / 2);
        let mw = fw.translate(rw);
        
        this.setLocation(this.location.translate(mw));

        return mw;
    }

    vision(fov = this.fov) {
        let res = [];

        this.getRegion().content.forEach((e) => {
            if (e.id === this.id) return;

            if (this.canSee(e, fov)) {
                res.push({
                    entity: e,
                    angle: this.relAngle(e),
                    distance: e.location.distance(this.location)
                });
            }
        });

        return res;
    }

    angleTo(other) {
        let res = other.location.translate(this.location.neg());

        return res.angle;
    }

    extraProps() {
        let dp = super.extraProps();

        dp.angle = this.angle;
        dp.fov = this.fov;
        dp.groundSpeed = this.groundSpeed;
        dp.moveDest = this.moveDest.toData();
        dp.angleDest = this.angleDest;

        return dp;
    }

    static _fromData(d) {
        let e = new SMWorld.AngledLocalEntity(d.name, SMWorld.AnyPolarPoint.fromData(d.location), d.props.region, d.radius, d.solid, d.integrity, d.props.angle, d.props.fov, d.props.groundSpeed, d.id);
        if (d.audition != null) e.audition = d.audition;
        if (d.props.moveDest != null) e.moveDest = SMWorld.AnyPolarPoint.fromData(d.props.moveDest);
        if (d.props.angleDest != null) e.angleDest = d.props.angleDest;
        
        return e;
    }
});

SMWorld.Door = addType(class Door extends SMWorld.LocalEntity {
    constructor(name, region, opts, rloc, rsize, integrity = 50, id = null) {
        super(name, rloc.translate(new SMWorld.AnyPolarPoint(rsize, opts.angle)), region, opts.radius != null ? opts.radius : 20, false, integrity, id);

        this.angle = opts.angle;
        this.angle -= Math.floor(this.angle / Math.PI / 2) * Math.PI * 2;
        this.from = region;
        this.to = opts.to;
    }

    static create(name, regionObj, to, angle, radius, integrity = 50, id = null) {
        return new Door(name, regionObj.id, { to: to.id, angle: angle, radius: radius }, regionObj.location, regionObj.radius, integrity, id);
    }

    static _fromData(d) {
        let e = new SMWorld.Door(d.name, d.props.region, { to: d.props.to, angle: d.props.angle, radius: d.radius }, SMWorld.AnyPolarPoint.fromData(d.props.rloc), d.props.rsize, d.integrity, d.id);
        if (d.audition != null) e.audition = d.audition;
        
        return e;
    }

    getDest() {
        return SMWorld.Region.all.get(this.to);
    }

    bumped(e) {
        if (e.traveling) {
            let newLoc = e.getRegion().location.translate(e.location.neg());
            newLoc = new SMWorld.AnyPolarPoint(e.radius + this.radius + 2, newLoc.angle);

            e.setLocation(this.location.translate(newLoc));
        }

        else {
            if (e.constructor === SMWorld.Illusion || e.constructor === SMWorld.Door) return;
        
            let dt = this.getDest();
            e.region = dt.id;

            e.traveling = true;
            e.setAngle(this.angle);

            let destLoc = dt.edgeAtAngle(this.angleOffset(Math.PI), e.radius + 5);

            e.setLocation(destLoc);
            e.emit('status message', `You travel through ${this.name}, and reach ${this.getDest().name}.`);
            console.log(`${e.name} travels through ${this.name}, and reaches ${this.getDest().name} at ${destLoc.toString()}.`);
        }
    }

    angleOffset(a) {
        return (this.angle + a) - Math.floor((this.angle + a) / Math.PI / 2) * Math.PI / 2;
    }

    extraProps() {
        let dp = super.extraProps();
        dp.to = this.to;
        dp.rloc = this.getRegion().location.toData();
        dp.rsize = this.getRegion().radius;
        dp.angle = this.angle;
        
        return dp;
    }
});
SMWorld.Region = addType(class Region extends SMWorld.Entity {
    constructor(name, x, y, size, integrity = 100000, starting = false, id = null) {
        super(name, SMWorld.AnyPolarPoint.fromCartesian(x, y), size, false, integrity, id);
        this.content = new Map();
        this.starting = starting;
        SMWorld.Region.all.set(this.id, this);
    }

    edgeAtAngle(a, margin = 0) {
        return this.localToGlobal(new SMWorld.AnyPolarPoint(this.radius - margin, a));
    }

    inside(e) {
        return e.localPos().dist <= this.radius - e.radius;
    }

    pointInside(p) {
        return p.distance(this.location) <= this.radius;
    }

    fit(entity) {
        if (isNaN(entity.location.x) || isNaN(entity.location.y) || isNaN(entity.location.dist) || isNaN(entity.location.angle)) {
            console.warn(`-------\nNaN found in entity coordinate! Resetting it to region centre.\nEntity: ${entity.name} (of type ${entity.constructor.name})\n-------`);
            entity.setLocation(this.location);
        }

        else {
            let l = entity.localPos();
            l = new SMWorld.AnyPolarPoint(this.radius - entity.radius - 5, l.angle);
            
            entity.setLocation(this.localToGlobal(l));
        }

        if (entity.angle && entity.setAngle)
            entity.setAngle(this.location.sub(entity.location).angle);
    }

    extraProps() {
        let dp = super.extraProps();
        dp.starting = this.starting;
        return dp;
    }

    static resetAll() {
        SMWorld.Region.all = new Map();
    }

    static _fromData(d) {
        let l = SMWorld.AnyPolarPoint.fromData(d.location);
        let e = new Region(d.name, l.x, l.y, d.radius, d.integrity, d.props.starting, d.id);
        
        return e;
    }

    localToGlobal(point) {
        return point.translate(this.location);
    }

    globalToLocal(point) {
        return point.translate(this.location.neg());
    }
});

SMWorld.RectangularRegion = addType(class RectangularRegion extends SMWorld.Region {
    constructor(name, x, y, width, height, integrity = 100000, starting = false, id = null) {
        super(name, x, y, Math.max(width / 2, height / 2), integrity, starting, id);
        this.width = width;
        this.height = height;

        SMWorld.Region.all.set(this.id, this);
    }

    fit(e) {
        let box = this.boundingBox();

        if (e.location.x <= box.x1 + e.radius) {
            e.setLocation(SMWorld.AnyPolarPoint.fromCartesian(box.x1 + e.radius + 5, e.location.y));
            if (e.angle != null && e.setAngle) e.setAngle(0);
        }

        if (e.location.x >= box.x2 - e.radius) {
            e.setLocation(SMWorld.AnyPolarPoint.fromCartesian(box.x2 - e.radius - 5, e.location.y));
            if (e.angle != null && e.setAngle) e.setAngle(Math.PI);
        }

        if (e.location.y <= box.y1 + e.radius) {
            e.setLocation(SMWorld.AnyPolarPoint.fromCartesian(e.location.x, box.y1 + e.radius + 5));
            if (e.angle != null && e.setAngle) e.setAngle(Math.PI * 3 / 2);
        }

        if (e.location.y >= box.y2 - e.radius) {
            e.setLocation(SMWorld.AnyPolarPoint.fromCartesian(e.location.x, box.y2 - e.radius - 5));
            if (e.angle != null && e.setAngle) e.setAngle(Math.PI / 2);
        }
    }

    inside(e) {
        let box = this.boundingBox();

        return (
            e.location.x >= box.x1 + e.radius &&
            e.location.x <= box.x2 - e.radius &&
            e.location.y >= box.y1 + e.radius &&
            e.location.y <= box.y2 - e.radius
        );
    }

    boundingBox() {
        return {
            x1: this.location.x - this.width / 2,
            x2: this.location.x + this.width / 2,
            y1: this.location.y - this.height / 2,
            y2: this.location.y + this.height / 2
        };
    }

    extraProps() {
        let dp = super.extraProps();
        dp.width = this.width;
        dp.height = this.height;

        return dp;
    }

    static resetAll() {
        
    }

    static _fromData(d) {
        let l = SMWorld.AnyPolarPoint.fromData(d.location);
        let e = new RectangularRegion(d.name, l.x, l.y, d.props.width, d.props.height, d.integrity, d.props.starting, d.id);
        
        return e;
    }
});

SMWorld.getSave = function() {
    let res = {
        types: [],
        entities: Array.from(SMWorld.Entity.all.entries()).map((en) => {
            let e = en[1];

            return e.toData();
        })
    };

    SMWorld.typeIndex.forEach((t, i) => {
        res.types.push({ name: t.name, id: i });
    });

    return res;
};

SMWorld.loadSave = function(save) {
    SMWorld.typeIndex.forEach((t) => {
        t.resetAll();
    });

    SMWorld.typeIndex = new Map();

    save.types.forEach((t) => {
        SMWorld[t.name].id = t.id;
        SMWorld.typeIndex.set(t.id, SMWorld[t.name]);
    });

    save.entities.forEach((ent) => {
        SMWorld.Entity.fromData(ent);
    });

    SMWorld.Entity.all.forEach((e) => {
        e.postInit();
    });
};

SMWorld.AnimalEntity = addType(class AnimalEntity extends SMWorld.AngledLocalEntity {
    constructor(name, location, region, options, id = null) {
        //                              ^^^^^^^
        // MY GOD I'M TIRED OF THESE HUGE CONSTRUCTOR ARGUMENT LISTS!
        // This is not a convenience, this is a NECESSITY!!
        let { radius, solid, integrity, angle, sounds, audition, fov, groundSpeed } = options;

        // default values
        integrity = integrity || 100;
        audition = audition || 0;
        angle = angle || Math.random() * Math.PI * 2;
        radius = radius || 1.3;
        solid = (solid != null ? solid : true);
        sounds = sounds || {};
        fov = fov || Math.PI / 2;

        super(name, location, region, radius, solid, integrity, angle, fov, groundSpeed, id);

        this.audition = audition;
        this.sounds = sounds;
        this.state = options.state || 'idle';
        this.aggressive = options.aggressive || false;
        this.meleeDamage = options.meleeDamage || 5;
        this.target = options.target || null;
    }

    damage(points, source) {
        this.integrity -= points;

        this.emit('status event', `You get damaged by ${source != null && this.canSee(source) ? source.name : 'something'}!`);

        if (source != null)
            SMWorld.playSound(this, points, `${source.name} attacking what seems to be ${this.name}...`);
            
        else
            SMWorld.playSound(this, points, `${['painful', 'agonizing', 'screeching', 'damaged', 'squealing'][Math.floor(5 * Math.random())]} sound from what seems to be ${this.name}...`);

        if (this.integrity <= 0)
            this.destroy(points);
            
        else if (this.aggressive) {
            this.target = source.id;
            this.state = 'chase';
        }
    }

    getTarget() {
        return SMWorld.Entity.all.get(this.target);
    }

    tick(dt) {
        super.tick(dt);

        if (this.state === 'idle' && this.sounds.idle != null && Math.pow(Math.random(), 1 / dt) > 0.8) {
            SMWorld.playSound(this, 50, this.sounds.idle[Math.floor(this.sounds.idle.length * Math.random())]);
        }

        if (this.state === 'idle' && Math.pow(Math.random(), 1 / dt) >= 0.8) {
            this.state = 'roam';
            return;
        }

        if (this.state !== 'idle') {
            if (this.state === 'roam') {
                this.move(dt * Math.random() * 15, dt * (Math.random() * 1 - 0.1));
                this.addAngle(dt * (Math.random() * Math.PI / 4 - Math.PI / 8));

                if (this.sounds.idle != null && Math.pow(Math.random(), 1 / dt) > 0.9)
                    SMWorld.playSound(this, 50, this.sounds.idle[Math.floor(this.sounds.idle.length * Math.random())]);

                if (Math.pow(Math.random(), 1 / dt) >= 0.8)
                    this.state = 'idle';
            }
            
            else if (this.state === 'melee') {
                let targ = SMWorld.Entity.all.get(this.target);
                
                if (this.region !== targ.region)
                    this.state = 'roam';

                if (this.location.distance(targ.location) < this.radius * 1.5) {
                    targ.damage(this.meleeDamage);
                    this.move(-3, 0);
                    this.state = 'chase';
                }
            }

            else if (this.state === 'chase') {
                let targ = SMWorld.Entity.all.get(this.target);

                if (targ == null) {
                    this.state = 'roam';
                    return;
                }

                if (this.sounds.angry != null && Math.pow(Math.random(), 1 / dt) > 0.6) {
                    SMWorld.playSound(this, 50, this.sounds.angry[Math.floor(this.sounds.angry.length * Math.random())]);
                }

                if (this.region !== targ.region)
                    this.state = 'roam';

                else {
                    let ang = this.relAngle(targ);

                    this.addAngle(dt * ang / 3);

                    if (this.location.distance(targ.location) < 1.5)
                        this.state = 'melee';

                    else if (ang === 0 || this.location.distance(targ.location) * Math.tan(ang) < 1)
                        this.move(dt * 60, dt * -ang / 2);

                    else
                        this.move(dt * 30, dt * -ang / 1.2);
                }
            }
        }
    }

    extraProps() {
        let dp = super.extraProps();

        dp.sounds = this.sounds;
        dp.state = this.state;
        dp.aggressive = this.aggressive;
        dp.target = this.target;

        return dp;
    }

    static _fromData(d) {
        let e = new AnimalEntity(d.name, SMWorld.AnyPolarPoint.fromData(d.location), d.props.region, {
            integrity: d.integrity,
            audition: d.audition,
            angle: d.props.angle,
            radius: d.radius,
            solid: d.solid,
            sounds: d.props.sounds,
            state: d.props.state,
            aggressive: d.props.aggressive,
            groundSpeed: d.props.groundSpeed,
            target: d.props.target
        }, d.id);
        if (d.props.moveDest != null) e.moveDest = SMWorld.AnyPolarPoint.fromData(d.props.moveDest);
        if (d.props.angleDest != null) e.angleDest = d.props.angleDest;
        
        return e;
    }
});

SMWorld.Illusion = addType(class Illusion extends SMWorld.AngledLocalEntity {
    tick(dt) {
        this._addAngle(dt * (Math.random() * 2 - 1) * Math.PI / 5);
        this._move((15 + Math.random() * 15) * dt, dt * (Math.random() * 10 - 5));
    }

    static _fromData(d) {
        let e = new SMWorld.Illusion(d.name, SMWorld.AnyPolarPoint.fromData(d.location), d.props.region, d.radius, d.solid, d.integrity, d.props.angle, d.props.fov, d.props.groundSpeed, d.id);
        if (d.audition != null) e.audition = d.audition;
        if (d.props.moveDest != null) e.moveDest = SMWorld.AnyPolarPoint.fromData(d.props.moveDest);
        if (d.props.angleDest != null) e.angleDest = d.props.angleDest;
        
        return e;
    }
});

SMWorld.PlayerEntity = addType(class PlayerEntity extends SMWorld.AngledLocalEntity {
    constructor(name, location, region, radius, solid, integrity, angle, fov, groundSpeed, id = null) {
        super(name, location, region, radius, solid, integrity, angle, fov, groundSpeed, id);
        this.audition = 0;
    }

    static _fromData(d) {
        let e = new SMWorld.PlayerEntity(d.name, SMWorld.AnyPolarPoint.fromData(d.location), d.props.region, d.radius, d.solid, d.integrity, d.props.angle, d.props.fov, d.props.groundSpeed, d.id);        
        if (d.props.moveDest != null) e.moveDest = SMWorld.AnyPolarPoint.fromData(d.props.moveDest);
        if (d.props.angleDest != null) e.angleDest = d.props.angleDest;
        return e;
    }
});

SMWorld.createPlayer = function createPlayer(name, randomizeNext = true) {
    let e = new SMWorld.PlayerEntity(name, SMWorld.start.region.location.translate(SMWorld.start.offset), SMWorld.start.region.id, 2.2, true, 100, Math.random() * Math.PI * 2, Math.PI / 1.8, 70);
    e.postInit();

    if (randomizeNext) SMWorld.randomStart();

    return e;
};

SMWorld.Region.all = new Map();

SMWorld.tickAll = function tickAll(deltaSecs) {
    SMWorld.Entity.all.forEach((e) => {
        e.tick(deltaSecs);
    });
};

SMWorld.randomStart = function randomStart() {
    let regs = Array.from(SMWorld.Region.all.entries()).map((e) => e[1]).filter((r) => r.starting);
    let reg = regs[Math.floor(Math.random() * regs.length)];

    SMWorld.start = {
        region: reg,
        offset: new SMWorld.AnyPolarPoint(Math.random() * reg.radius, Math.random() * Math.PI * 2)
    };
};

SMWorld.playSound = function playSound(origin, intensity, sound) {
    SMWorld.Entity.all.forEach((e) => {
        // volume in decibels, 0 = human hearing threshold
        let dist = e.location.distance(origin.location);
        let attenuated = intensity;

        if (dist > 0)
            attenuated -= 4 * Math.log(Math.pow(dist, 2)) / Math.log(10);

        if (e.audition != null && attenuated >= e.audition)
            e.emit('sound', sound, attenuated);
    });
};

SMWorld.applyDelta = function applyDelta(d) {
    let save = SMWorld.getSave();
    changesets.applyChanges(save, d.diffs);

    let checksum = strsum(JSON.stringify(save));
    if (checksum === d.checksum) SMWorld.loadSave(save);

    return checksum === d.checksum;
};

SMWorld.generateDelta = function generateDelta(previous) {
    let save = SMWorld.getSave();

    return {
        diffs: changesets.diff(previous, save),
        checksum: strsum(JSON.stringify(save))
    };
};

module.exports = SMWorld;