// Credits: https://stackoverflow.com/a/7616484
function hashCode(s) {
    var hash = 0, i, chr;
    if (s.length === 0) return hash;

    for (i = 0; i < this.length; i++) {
        chr   = s.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }

    return hash;
}

module.exports = hashCode;