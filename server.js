#!/usr/bin/env node
const util = require('util');
const SMWorld = require('.');
const flaker = require('./flaker.js')();
const fs = require('fs');
const express = require('express');
const socketio = require('socket.io');
const http = require('http');
const path = require('path');

const tickRate = 12;

let sifn = path.join('saves', 'index.json');
let saveIndex = { name: null, last: null, saves: [] };

if (!fs.existsSync('saves'))
    fs.mkdirSync('saves');

if (fs.existsSync(sifn))
    saveIndex = JSON.parse(fs.readFileSync(sifn, 'utf-8'));

else
    fs.writeFileSync(sifn, JSON.stringify(saveIndex));

let words = ['above-mentioned', 'above-listed', 'before-mentioned', 'aforementioned', 'abundance', 'accelerate', 'accentuate', 'accommodation', 'accompany', 'accomplish', 'accorded', 'accordingly', 'accrue', 'accurate', 'acquiesce', 'acquire', 'additional', 'address', 'addressees', 'adjustment', 'admissible', 'advantageous', 'advise', 'aggregate', 'aircraft', 'alleviate', 'allocate', 'alternatively', 'ameliorate', 'and/or', 'anticipate', 'applicant', 'application', 'apparent', 'apprehend', 'appreciable', 'appropriate', 'approximate', 'ascertain', 'attain', 'attempt', 'authorize', 'beg', 'belated', 'beneficial', 'bestow', 'beverage', 'capability', 'caveat', 'cease', 'chauffeur', 'clearly', 'obviously', 'combined', 'commence', 'complete', 'component', 'comprise', 'conceal', 'concerning', 'consequently', 'consolidate', 'constitutes', 'contains', 'convene', 'corridor', 'currently', 'deem', 'delete', 'demonstrate', 'depart', 'designate', 'desire', 'determine', 'disclose', 'different', 'discontinue', 'disseminate', 'duly', 'authorized', 'signed', 'each...apiece', 'economical', 'elect', 'eliminate', 'elucidate', 'emphasize', 'employ', 'encounter', 'endeavor', 'end', 'result', 'product', 'enquiry', 'ensure', 'entitlement', 'enumerate', 'equipments', 'equitable', 'equivalent', 'establish', 'evaluate', 'evidenced', 'evident', 'evince', 'excluding', 'exclusively', 'exhibit', 'expedite', 'expeditious', 'expend', 'expertise', 'expiration', 'facilitate', 'fauna', 'feasible', 'females', 'finalize', 'flora', 'following', 'forfeit', 'formulate', 'forward', 'frequently', 'function', 'furnish', 'grant', 'herein', 'heretofore', 'herewith', 'thereof', 'wherefore', 'wherein', 'however', 'identical', 'identify', 'immediately', 'impacted', 'implement', 'inasmuch', 'inception', 'indicate', 'indication', 'initial', 'initiate', 'interface', 'irregardless', 'liaison', '-ly', 'doubtless', 'fast', 'ill', 'much', 'seldom', 'thus', 'magnitude', 'maintain', 'majority', 'maximum', 'merge', 'methodology', 'minimize', 'minimum', 'modify', 'monitor', 'moreover', 'multiple', 'necessitate', 'nevertheless', 'notify', 'not...unless', 'not...except', 'not...until', 'notwithstanding', 'numerous', 'objective', 'obligate', 'observe', 'obtain', 'operate', 'optimum', 'option', 'orientate', '...out', 'calculate', 'cancel,', 'distribute', 'segregate', 'separate', 'overall', 'parameters', 'participate', 'particulars', 'perchance', 'perform', 'permit', 'perspire', 'peruse', 'place', 'portion', 'possess', 'potentiality', 'practicable', 'preclude', 'preowned', 'previously', 'prioritize', 'proceed', 'procure', 'proficiency', 'promulgate', 'provide', 'purchase', 'reflect', 'regarding', 'relocate', 'remain', 'remainder', 'remuneration', 'render', 'represents', 'request', 'require', 'requirement', 'reside', 'residence', 'respectively', 'retain', 'retire', 'rigorous', 'selection', 'separate', 'shall', 'solicit', 'state-of-the-art', 'strategize', 'subject', 'submit', 'subsequent', 'subsequently', 'substantial', 'sufficient', 'terminate', 'therefore', 'therein', 'timely', 'transpire', 'transmit', 'type', 'validate', 'variation', 'very', 'viable', 'warrant', 'whereas', 'whosoever', 'whomsoever', 'witnessed'];

function memoryID(size = 5, maxSuffixSize = 8, wordSpace = words) {
    return new Array(size).fill(0).map(() => wordSpace[Math.floor(wordSpace.length * Math.random())]).join('-') + '-' + Math.ceil(Math.random() * 10 ^ maxSuffixSize);
}


class Server {
    emitDeltas() {
        this.clients.forEach((c) => {
            let s = SMWorld.getSave();

            if (c.previous == null) {
                let j = JSON.stringify(s);

                c.socket.emit('updateSnapshot', j);
            }

            else
                c.socket.emit('deltaSnapshot', SMWorld.generateDelta(c.previous));

            c.previous = s;
        });
    }

    save(name = new Date()) {
        name = name.toString();
        name = `${name}.json`;

        this.log(`%%% Autosaved world at ${path.join('saves', name)}.`);

        fs.writeFileSync(path.join('saves', name), JSON.stringify(this.getSave()));

        saveIndex.saves.push(name);
        saveIndex.last = saveIndex.saves.length - 1;

        fs.writeFileSync(sifn, JSON.stringify(saveIndex));
    }

    getSave() {
        return {
            world: SMWorld.getSave(),
            dormant: Array.from(this.dormant.entries()).map((e) => ({
                name: e[0],
                code: e[1].code,
                player: e[1].toData()
            }))
        };
    }

    constructor(port = 8805, options = {}) {
        this.clients = new Map();
        this.chatLog = [];
        
        let logFile = options.logFile || 'smworld-server.log';
        this.dormant = options.dormant != null ? options.dormant : new Map();

        this.logFile = logFile;
        this.app = express();
        this.server = http.createServer(this.app);
        this.io = socketio(this.server);
        this.previous = new Map();

        let turns = [];

        this.app.use(express.static('www'));

        this.io.on('connection', (socket) => {
            let req = socket.request;
            let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

            let id = 'unknown ID';
            let client = null;
            let player = null;

            this.log(`+++ New socket connected from ${ip}`);

            socket.on('requestSnapshot', () => {
                if (client == null) return;

                let s = SMWorld.getSave();
                let j = JSON.stringify(s);

                socket.emit('updateSnapshot', j);
                client.previous = s;
            });

            socket.on('authenticate', (msg) => {
                let status = {
                    code: 0,
                    success: true,
                    message: 'Connection successful.'
                };

                let data;

                try {
                    data = JSON.parse(msg);
                }

                catch(e) {
                    status = {
                        code: 4,
                        success: false,
                        message: 'Broken JSON data sent!'
                    };
                }

                if (status.success) {
                    if (!(data && data.name)) {
                        status = {
                            code: 3,
                            success: false,
                            message: 'Invalid JSON data sent!'
                        };
                    }

                    else {
                        let name = data.name;
                        let pcode = data.pcode || null;
                        
                        if (name === '' || name == null || typeof name !== 'string') {
                            status = {
                                code: 2,
                                success: false,
                                message: 'Invalid name!'
                            };
                        }

                        else if (Array.from(this.clients.entries()).some((user) => user[1].name === name)) {
                            status = {
                                code: 1,
                                success: false,
                                message: 'Name already taken!'
                            };
                        }

                        else {
                            if (this.dormant.has(name) && this.dormant.get(name).destroyed && pcode != null) {
                                if (pcode === this.dormant.get(name).code) {
                                    player = this.dormant.get(name);
                                    player.undestroy();
                                }

                                else {
                                    let dmt = this.dormant.get(name);
                                    this.log(`/!\ Player tried to login as dormant player ${dmt.name} (expected code '${dmt.code}', got '${pcode}' instead)!`);

                                    status = {
                                        code: 5,
                                        success: false,
                                        message: 'Invalid Player Code for awakening!'
                                    };
                                }
                            }

                            else {
                                player = SMWorld.createPlayer(name);
                                player.code = memoryID(3, 4);
                                
                                if (this.dormant.has(name))
                                    this.dormant.delete(name);
                                    
                                this.dormant.set(player.name, player);
                            }
                            
                            if (status.success) {
                                client = new Client(name, ip, socket);
                                this.clients.set(client.id, client);
                                
                                status.password = id = client.id;
                                status.players = [];
                                
                                SMWorld.Entity.all.forEach((e) => {
                                    if (e.constructor === SMWorld.PlayerEntity && e.name !== name)
                                        status.players.push(e.name);
                                });
                                
                                status.you = player.id;
                                status.playerCode = player.code;
                                turns.push(player);
                                
                                player.removeAllListeners('status message');
                                player.removeAllListeners('sound');
                                
                                player.on('status message', (msg) => {
                                    this.log(`@@@ (updated ${player.name}'s status message) ${msg}`)
                                    socket.emit('status message', msg);
                                });
                                
                                player.on('sound', (msg, level) => {
                                    socket.emit('sound', msg, level);
                                });
                                
                                SMWorld.playSound(player, 58, 'Fwoop!');
                                
                                this.log(`*** Socket from ${ip} is now known as '${name}' (client ID: ${client.id})! Authentication password generated.`);

                                status.snapshot = SMWorld.getSave();
                            }
                        }
                    }

                    socket.emit('authStatus', JSON.stringify(status));

                    if (status.success) {
                        this.io.emit('joined', JSON.stringify({
                            name: player.name,
                            entity: player.toData()
                        }));

                        this.chatLog.forEach((c) => {
                            socket.emit('chat', JSON.stringify(c));
                        });
                    }
                }   
            });

            socket.on('command', (msg) => {
                let command;

                try {
                    command = JSON.parse(msg);
                }

                catch (e) {
                    this.log(`!!! Broken command from socket ${ip}!`);
                    return;
                }

                if (!client) {
                    this.log(`!!! Bad client variable while running game command from socket ${ip}!`);
                    return;
                }

                if (!(command && command.password && command.type && command.args && typeof command.type === 'string' && Array.isArray(command.args))) {
                    this.log(`!!! Invalid command from socket ${ip}!`);
                    return;
                }

                if (command.password !== client.id) {
                    this.log(`!!! Socket from ${ip} attempted to hijack client of ID ${client.id}!`);
                    return;
                }

                if (command.type === 'move')
                    player.move.apply(player, command.args.map((n) => isNaN(+n) ? 0 : +n));

                else if (command.type === 'turn')
                    player.addAngle(Math.max(-120, Math.min(120, isNaN(+command.args[0]) ? 0 : +command.args[0])));

                this.log(`||| ${player.name} used command: ${command.type} (arguments: ${command.args.map(util.inspect).join(', ')})`);
                this.emitDeltas();
            });

            socket.on('chat', (message) => {
                try {
                    message = JSON.parse(message);
                }

                catch (e) {
                    return; // loser!
                }

                if (client == null || message.password != client.id) {
                    return;
                }

                let msg = message.message;

                if (typeof msg !== 'string') return;

                if (player != null) {
                    SMWorld.playSound(player, 62, `"${msg}"`);

                    let c = {
                        author: player.name,
                        content: msg
                    };

                    this.log(`    <${player.name}> ${msg}`);
                    this.io.emit('chat', JSON.stringify(c));
                    this.chatLog.push(c);
                }
            });

            socket.on('disconnect', () => {
                this.log(`--- Socket ${ip} (${id}) disconnected.`);

                if (player != null) {
                    this.io.emit('left', player.name, player.id);
                    
                    player.destroy(0);
                    this.clients.delete(client.id);
                }
            });
        });

        setInterval(() => {
            SMWorld.tickAll(1 / tickRate);
            this.emitDeltas();
        }, 1000 / tickRate);

        setInterval(() => {
            this.save();
        }, 10 * 60000);

        this.server.listen(port, function() { console.log(`... Listening website at localhost:${port}.`); });
    }

    log(line) {
        line = `[${new Date()}] ${line}`;
        fs.appendFileSync(this.logFile, line + '\r\n');
        console.log(line);
    }
}

class Client {
    constructor(name, ip, socket) {
        this.name = name;
        this.ip = ip;
        this.socket = socket;

        this.id = flaker();
    }
}


let args = process.argv.slice(2);
let fn = args.shift();

if (fn != null && !fn.match(/^\d+$/)) {
    let world;
    let dormant = new Map();

    if (saveIndex.last == null || saveIndex.name != fn) {
        world = JSON.parse(fs.readFileSync(fn, 'utf-8'));
        SMWorld.loadSave(world);
    }

    else {
        let save = JSON.parse(fs.readFileSync(path.join('saves', saveIndex.saves[saveIndex.last]), 'utf-8'));
        world = save.world;
        saveIndex.name = fn;
        fs.writeFileSync(sifn, JSON.stringify(saveIndex));
        
        SMWorld.loadSave(world);
     
        save.dormant.forEach((entry) => {
            let player = SMWorld.Entity.all.get(entry.player.id);
            player.code = entry.code;

            dormant.set(entry.name, player);
        });
    }
    SMWorld.randomStart();

    new Server(args.shift(), {
        dormant: dormant
    });
}

else {
    console.log('WARNING: Requires a filename argument!');
}